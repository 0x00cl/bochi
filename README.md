# Bochi

Bochi es un pequeño script que permite capturar los nuevos comentarios que se hacen en https://reddit.com/r/chile y almacenarlos en MongoDB

## Requerimientos

* MongoDB 5+
* Python 3.7+

## Estructura

* src: Contiene el script de python que permite capturar los nuevos comentarios de /r/chile
* api: Contiene una API escrita en Python con Flask para las consultas a MongoDB de los comentarios

## Preparación mongodb

El script asume que mongodb tiene una base de datos llamado `rchile` con una colección llamada `comentarios`.

### Ejecución script

Primero se deben instalar los modulos de Python necesarios para realizar la conexión a la API de reddit y MongoDB

```bash
$ pip install -r requirements.txt
```

Luego se debe crear un archivo llamado constants.py que contiene los secretos para los tokens de reddit y la URL de conexión a la base de datos MongoDB

```python
settings = {
    "REDDIT_CLIENT_ID": "CLIENT_ID",
    "REDDIT_CLIENT_SECRET": "CLIENT_SECRET",
    "REDDIT_USERNAME": "USERNAME",
    "REDDIT_PASSWORD": "PASSWORD",
    "MONGODB_URL": "mongodb://user:pass@localhost:27017/?authMechanism=DEFAULT&tls=false",
}

```

También estas opciones se pueden configurar como variables de ambiente sin la necesidad de crear el archivo, como:
```shell
$ export REDDIT_USERNAME=myuser212
```
Finalmente se puede ejecutar de la siguiente forma:

```shell
$ python main.py
```

### Ejecución API

Primero se deben instalar los modulos de Python necesarios para realizar la conexión a MongoDB y levantar Flask

```bash
$ pip install -r requirements.txt
```

Luego se debe crear un archivo llamado constants.py que contiene los la URL de conexión a la base de datos MongoDB y el nivel de logs que se desea:

```python
settings = {
    "LOGGING_LEVEL": "INFO",
    "MONGODB_URL": "mongodb://user:pass@localhost:27017/?authMechanism=DEFAULT&tls=false"
}

```

También estas opciones se pueden configurar como variables de ambiente sin la necesidad de crear el archivo, como:
```shell
$ export LOGGING_LEVEL=ERROR
```
Finalmente se puede ejecutar de la siguiente forma:

```shell
$ cd api
$ export FLASK_APP=bochi_api
$ flask run
```

## Autor

Este script fue creado por [0x00cl](https://0x00.cl) (Tomás Gutiérrez).

Está bajo una licencia [GLPv3](https://www.gnu.org/licenses/gpl-3.0.html).