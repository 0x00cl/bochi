#!/usr/bin/env python
"""Script que procesa comentarios de /r/chile"""
import sys
from datetime import date, timedelta
import logging
import pymongo

logger = logging.getLogger(__name__)


class RchileComentarios:
    def __init__(self, url):
        try:
            mdb = pymongo.MongoClient(url, serverSelectionTimeoutMS=15000)
            mdb.admin.command("ping")
        except pymongo.errors.ConnectionFailure as err:
            logging.critical(
                "Falla al conectarse a MongoDB, terminando script. (%s)", err
            )
            sys.exit(1)
        else:
            logging.info("Conectado a mongoDB!")
        self.rchile = mdb["rchile"]

    def comentario_usuario(self, username):
        today = date.today()
        start = today - timedelta(days=today.weekday())
        end = start + timedelta(days=6)
        return {
            "total": self.rchile.comentarios.count_documents({"author": username, "created_utc": {"$gte": int(start.strftime("%s"))}}),
            "start_utc": int(start.strftime("%s"))
        }


def main():
    pass


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logging.warning("Script detenido")
        sys.exit(130)
    except Exception as err:
        logging.error("Ocurrió un error: %s", err)
        sys.exit(1)
