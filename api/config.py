import sys
import os
import logging

BOT_NAME = "Bochi api"
BOT_VERSION = "0.1.0"
BOT_USERNAME = "0x00cl"
BOT_UA = f"{BOT_NAME} {BOT_VERSION} by /u/{BOT_USERNAME}"

settings = {
    "MONGODB_URL": None,
    "LOGGING_LEVEL": "INFO"
}


try:
    import constants

    CONSTANTS_IMPORTED = True
except ImportError:
    CONSTANTS_IMPORTED = False

logging.basicConfig(
    format="%(asctime)s [%(levelname)-8s] (%(funcName)s:%(lineno)d) %(message)s",
    level=settings["LOGGING_LEVEL"],
)

no_settings = []

for value in settings.copy():
    if os.environ.get(value):
        settings[value] = os.environ.get(value)
    elif CONSTANTS_IMPORTED:
        try:
            settings[value] = constants.settings[value]
        except AttributeError:
            logging.critical(
                "Archivo constants.py importado pero no contiene la variable 'settings'"
            )
            sys.exit(1)
        except KeyError:
            if value == "LOGGING_LEVEL":
                continue
            logging.error(
                "La configuración '%s' no existe en la variable 'settings', porfavor configurar.",
                value,
            )
            sys.exit(1)
    else:
        no_settings.append(value)
if no_settings:
    logging.critical("Falta configurar las siguientes opciones: %s", str(no_settings))
    sys.exit(1)

logging.getLogger().setLevel(settings["LOGGING_LEVEL"])
