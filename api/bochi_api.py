#!/usr/bin/env python
import logging
import flask
import mongo_queries
import config

app = flask.Flask(__name__)
logging.getLogger("werkzeug").setLevel(config.settings["LOGGING_LEVEL"])

mongo_conn = mongo_queries.RchileComentarios(config.settings["MONGODB_URL"])


@app.route("/")
def hello():
    return "<h1>Hello, World!</h1>"


@app.route("/comentarios/<username>")
def comentarios_usuario(username):
    return mongo_conn.comentario_usuario(username)
