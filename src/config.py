import sys
import os
import logging
import argparse

BOT_NAME = "Bochi"
BOT_VERSION = "0.1.0"
BOT_USERNAME = "0x00cl"
BOT_UA = f"{BOT_NAME} {BOT_VERSION} by /u/{BOT_USERNAME}"


def setup_argparse():
    """
    Sets up the arguments for the program

    Returns:
        args (argparse.Namespace): parsed arguments
    """
    parser = argparse.ArgumentParser(
        description="Script que procesa comentarios nuevos en /r/chile",
        epilog="""Copyright (C) 2022 Tomás Gutiérrez.
                This is free software, licensed under GPLv3.""",
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="Configura nivel de logs a DEBUG"
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"{BOT_UA}",
    )
    return parser.parse_args()


def setup_logging(debug):
    """
    Sets up logging module, setting log format and level

    Parameters:
        debug (bool): whether to show debug level in logs or not
    """
    log_level = logging.INFO
    if debug:
        log_level = logging.DEBUG
    logging.basicConfig(
        format="%(asctime)s [%(levelname)-8s] (%(funcName)s:%(lineno)d) %(message)s",
        level=log_level,
    )


args = setup_argparse()
setup_logging(args.debug)

settings = {
    "REDDIT_CLIENT_ID": None,
    "REDDIT_CLIENT_SECRET": None,
    "REDDIT_USERNAME": None,
    "REDDIT_PASSWORD": None,
    "MONGODB_URL": None,
}

try:
    import constants

    CONSTANTS_IMPORTED = True
except ImportError:
    CONSTANTS_IMPORTED = False

no_settings = []

for value in settings:
    if os.environ.get(value):
        settings[value] = os.environ.get(value)
    elif CONSTANTS_IMPORTED:
        try:
            settings[value] = constants.settings[value]
        except AttributeError:
            logging.error(
                "Archivo constants.py importado pero no contiene la variable 'settings'"
            )
            no_settings.append(value)
    else:
        no_settings.append(value)
if no_settings:
    logging.critical("Falta configurar las siguientes opciones: %s", str(no_settings))
    sys.exit(1)
