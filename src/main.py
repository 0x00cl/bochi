#!/usr/bin/env python
"""Script que captura todos los nuevos comentarios de /r/chile"""
import sys
import logging
import prawcore
import praw
import pymongo
import config

logger = logging.getLogger(__name__)


def main():
    """Main starting point"""
    logging.info("Iniciando script")
    reddit = praw.Reddit(
        client_id=config.settings["REDDIT_CLIENT_ID"],
        client_secret=config.settings["REDDIT_CLIENT_SECRET"],
        user_agent=f"{config.BOT_UA}",
        username=config.settings["REDDIT_USERNAME"],
        password=config.settings["REDDIT_PASSWORD"],
    )

    logging.info("Conectando a mongoDB.")
    try:
        mdb = pymongo.MongoClient(
            config.settings["MONGODB_URL"], serverSelectionTimeoutMS=15000
        )
        mdb.admin.command("ping")
    except pymongo.errors.ConnectionFailure as err:
        logging.critical("Falla al conectarse a MongoDB, terminando script. (%s)", err)
        sys.exit(1)
    else:
        logging.info("Conectado a mongoDB!")

    logging.info("Capturando commentarios de r/chile")
    try:
        for comment in reddit.subreddit("chile").stream.comments():
            mdb.rchile.comentarios.insert_one(
                {
                    "author": comment.author.name,
                    "body": comment.body,
                    "body_html": comment.body_html,
                    "created_utc": comment.created_utc,
                    "link_title": comment.link_title,
                    "link_author": comment.link_author,
                    "id": comment.id,
                    "parent_id": comment.parent_id,
                    "is_submitter": comment.is_submitter,
                    "link_id": comment.link_id,
                    "link_permalink": comment.link_permalink,
                    "link_url": comment.link_url,
                }
            )
    except prawcore.exceptions.ResponseException as err:
        logging.error("Error al autenticar: %s", err)
        mdb.close()
        sys.exit(1)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logging.warning("Bot detenido")
        sys.exit(130)
    except Exception as err:
        logging.error("Ocurrió un error: %s", err)
        sys.exit(1)
